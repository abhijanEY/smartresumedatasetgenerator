/**
 * 
 */
package data;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Stream;

/**
 * @author Krishnan.Jagadesan
 *
 */
public class ModelDataGenerator {

	private static final int matchThreshold = 70;
	private static final int numberOfPreferredSkills = 3;
	private static final int numberOfExperienceLevels = 3;
	private static final int numberOfCandidatesRequiredInDataSet = 40000;
	private static Map<String, ArrayList<String>> rolesToSkillsMap = new HashMap<String, ArrayList<String>>();
	private static Random random = new Random();

	public static void main(String[] args) throws Exception {
		ArrayList<Candidate> modelDataCandidates = new ArrayList<Candidate>();

		ModelDataGenerator modelDataGenerator = new ModelDataGenerator();
		modelDataGenerator.buildRolesToSkillsMap();

		List<Integer> numberOfCandidatesForEachRoleArray = getRandomNumbersList(numberOfCandidatesRequiredInDataSet,
				rolesToSkillsMap.keySet().size());

		int j = 0;
		for (String aRole : rolesToSkillsMap.keySet()) {
			for (int i = 0; i < numberOfCandidatesForEachRoleArray.get(j); i++)
				modelDataCandidates.add(modelDataGenerator.getOneHistoricalRecord(aRole));
			j++;
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter("ModelCandidatesData.csv"));
		String header_column = "Role,SkillsMatchPercentage,Level,Skill1,Skill2,Skill3,Selected";
		writer.write(header_column);
		writer.newLine();

		for (Candidate aCandidate : modelDataCandidates) {
			writer.write(aCandidate.toString());
			writer.newLine();
		}

		writer.close();
	}

	private static ArrayList<String> getRolesToSkillsMap(String aRole) {
		return rolesToSkillsMap.get(aRole);
	}

	private Map<String, ArrayList<String>> buildRolesToSkillsMap() throws Exception {
		/*
		 * ArrayList<String> skillsList; Properties rolesToSkillsConfiguration = new
		 * Properties(); rolesToSkillsConfiguration.load(new
		 * FileReader("RolesToSkillsMappings.txt"));
		 * 
		 * String roleName = ""; Set<Entry<Object, Object>> allRolesToSkills =
		 * rolesToSkillsConfiguration.entrySet();
		 * 
		 * for (Entry<Object, Object> entry : allRolesToSkills) { roleName = (String)
		 * entry.getKey(); skillsList = new ArrayList<String>(); for (String skills :
		 * ((String)entry.getValue()).split(",")) { skillsList.add(skills); }
		 * rolesToSkillsMap.put(roleName, skillsList); }
		 */

		/*String roleName = "";
		final String line = "";*/
		//String skillsList[];
	
		ArrayList<String> skills = new ArrayList<String>();

	
		
		
	
		/*while (line != null || ! line.isEmpty()) {		
			System.out.println(bufferedReader.readLine());
			
			String total[] = bufferedReader.readLine().split("=");
			roleName = total[0];
			skillsList = total[1].split(",");

			for (String skill : skillsList) {
				skills.add(skill);
			}
			rolesToSkillsMap.put(roleName, skills);
		}
		*/
		
		try (Stream<String> stream = Files.lines(Paths.get("RolesToSkillsMappings.txt"))) {

			stream.forEach( s -> {
				String total[] = s.split("=");
				String roleName = total[0];
				String[] skillsList = total[1].split(",");

				for (String skill : skillsList) {
					skills.add(skill);
				}
				rolesToSkillsMap.put(roleName, skills);
			}

					);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return rolesToSkillsMap;
	}

	public Candidate getOneHistoricalRecord(String aRole) {
		boolean candidateSelected = true;
		ArrayList<String> skillsListForThisRole = getRolesToSkillsMap(aRole);

		long skillsMatchPercentage = 0;
		if (candidateSelected) {
			skillsMatchPercentage = ThreadLocalRandom.current().nextLong(0, 100 + 1);
		}

		int experienceLevel = random.nextInt(numberOfExperienceLevels);

		Set<Integer> randomSet = new HashSet<>();
		while (randomSet.size() < numberOfPreferredSkills)
			randomSet.add(random.nextInt(skillsListForThisRole.size()));
		List<String> candidatesPreferredSkills = new ArrayList<String>();
		for (Integer i : randomSet) {
			candidatesPreferredSkills.add(skillsListForThisRole.get(i));
		}

		if (skillsMatchPercentage <= 40) {
			candidateSelected = false;
		} else {
			candidateSelected = true;
		}

		Candidate aHistoricalCandidate = new Candidate(aRole, skillsMatchPercentage, experienceLevel, candidateSelected,
				candidatesPreferredSkills);

		return aHistoricalCandidate;
	}

	public static List<Integer> getRandomNumbersList(int targetSum, int numberofListElements) {
		Random r = new Random();
		List<Integer> randomNumbersList = new ArrayList<>();

		int sum = 0;
		for (int i = 0; i < numberofListElements; i++) {
			int next = r.nextInt(targetSum) + 1;
			randomNumbersList.add(next);
			sum += next;
		}

		double scale = 1d * targetSum / sum;
		sum = 0;
		for (int i = 0; i < numberofListElements; i++) {
			randomNumbersList.set(i, (int) (randomNumbersList.get(i) * scale));
			sum += randomNumbersList.get(i);
		}

		while (sum++ < targetSum) {
			int i = r.nextInt(numberofListElements);
			randomNumbersList.set(i, randomNumbersList.get(i) + 1);
		}

		return randomNumbersList;

	}

	class Candidate {
		String role;
		long skillsMatchPercentage;
		int experienceLevel; // 1 - Junior, 2 - Mid, 3 - Senior
		boolean selected;
		List<String> preferredSkills = new ArrayList<String>();

		public Candidate(String role, long skillsMatchPercentage, int experienceLevel, boolean selected,
				List<String> preferredSkills) {
			super();
			this.role = role;
			this.skillsMatchPercentage = skillsMatchPercentage;
			this.experienceLevel = experienceLevel;
			this.selected = selected;
			this.preferredSkills = preferredSkills;
		}

		public String getRole() {
			return role;
		}

		public void setRole(String role) {
			this.role = role;
		}

		public long getSkillsMatchPercentage() {
			return skillsMatchPercentage;
		}

		public void setSkillsMatchPercentage(long skillsMatchPercentage) {
			this.skillsMatchPercentage = skillsMatchPercentage;
		}

		public int getExperienceLevel() {
			return experienceLevel;
		}

		public void setExperienceLevel(int experienceLevel) {
			this.experienceLevel = experienceLevel;
		}

		public boolean isSelected() {
			return selected;
		}

		public void setSelected(boolean selected) {
			this.selected = selected;
		}

		public List<String> getPreferredSkills() {
			return preferredSkills;
		}

		public void setPreferredSkills(List<String> preferredSkills) {
			this.preferredSkills = preferredSkills;
		}

		@Override
		public String toString() {
			StringBuffer preferredSkillsList = new StringBuffer();

			for (String skills : preferredSkills)
				preferredSkillsList.append(skills + ", ");

			return role + ", " + skillsMatchPercentage + ", " + experienceLevel + ", " + preferredSkillsList.toString()
					+ selected;
		}
	}

}

FullStack Engineer=Java,Microservice,Angular,Spring Boot,Bootstrap
UI Developer=Angular,React,Grunt,HTML 5,CSS,Javascript,Bootstrap
Python Engineer=Python,PIP,Flask,Django,AWS
Cloud Architect=AWS, Azure, Redshift,S3,DynamoDB
Database Administrator=MySQL,Oracle DBMS,MongoDB,Cassandra
Software Engineer=C#.net,ASP.net,Azure,jQuery,MSSQL,Bootstrap
Integration Engineer=Spring Boot,Mulesoft,TIBCO,API Gateway
Devops=Jenkins,Docker,Kubernetes,Python,perl,Shell scripting,Ansible